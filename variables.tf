#Azure variables

#This will get the subscriptionID from the terraform.tfvars
variable "subscriptionID" {
    type = string
    description = "Variable for our resource group"
}
#This will get the location from terraform.tfvars
variable "location" {
    type = string
    description = "location of your resource group"
}
#This will get the Network Interface ID the terraform.tfvars
variable "network_interface_id" {
    type = string
}
