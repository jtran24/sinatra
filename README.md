How to deploy Sinatra Simple App - Jane Tran

Prerequisites

An Azure account with a subscription
A resource group has been created in Azure (SinatraRGRP)
A network interface has been created in Azure (SinatraInt)
Terraform 
Visual Studio Code for easy editing file ( optional) 

Assumption:

You working on a Mac device or Windows 
You already have an Cloud Provider account ( this case I use Azure)
We will deploy in Linux based OS (Ubuntu 18 is used). 
You already have terraform installed and working.



Steps:

Deploy VM with terraform to Azure
Create a working folder called Sinatra ( this will have terraform files) and a folder APP ( this will store your application files Gemfile, config.ru, helloworld.rb)
Create the following terraform files in this folder

	Main.tf   (this file contains all the syntax that requires to build Azure resources)

	Terraform.tfvars ( Edit this file to put in  your Azure details such as subscription id, location, Network Interface)

	Variables.tf ( this file contains your Azure authentication variables)

From Sinatra folder run the following command from your terminal

	Terraform init  (this will get terraform to download the necessary provider for terraform and initialize them)

	Terraform plan (this is where your terraform validate your syntax along with the changes that will happen in your Azure resources)

It should come back with 8 resource to be created
	Plan: 8 to add, 0 to change, 0 to destroy.

From there you can run

	Terraform apply (this will perform the change)

		Type “y” to commit change
		
It will take about 30 second for all the resources to be created in your Azure environment
Refresh your Azure portal, within your resource group you will see all the resources you have listed in terraform

	Sinatra-publicIP
	Sinatra-vnet
	SinatraAPPVM
	Sinatradisk1
	SinatraInt
	SinatraSG

Now you need to associate public IP address with your SinatraInt (Network Interface) which attach to your VM
SSH to your SinatraAPPVM from your terminal ( use credentials you specified in the main.tf file)

	Ssh sinatra01@publicIPadress 
	Enter your password 
	
Set the firewall rule on your VM to have port 80 and 22 allow from terminal type:
	
	Sudo ufw allow http  ( this will allow port 80)
	Sudo ufw allow ssh (this will allow port 22)

	Sudo ufw enable (enable firewall under OS)
	Sudo ufw status ( this will return the active rules)
Because sinatra is a part of Ruby so we need to install ruby gem so Sinatra can be executed and run. From terminal run
		Sudo bundle install
		
	Then install sinatra gem
		Sudo gem install sinatra
Next step is copy your Sinatra app from your local laptop to this VM, from terminal run the following command

Mkdir Sinatra ( this will create a new folder for our app)

	Sudo chown -r Sinatra01 Sinatra ( this will grant user sinatra rw access to Sintra folder)

	Cd APP (access to your APP folder)

	Scp * sinatra01@publicIPaddress:/home/Sinatra01/Sinatra (this will copy all the files to Sinatra folder to your VM)
	Enter your password

	
Edit the helloworl.rb we need to add couple line to get the app run on port 80 (by default, Sinatra runs on port 4567) and can be access via public IP address
Under the line require ‘sinatra’

	set :bind, '0.0.0.0' #This is to bind to localhost
	set :port, 80 #This is to set the app run on port 80
	
	Save the file 
From terminal type to run the app
		Ruby helloworld.rb


From your computer open a web browser, browse to http://PublicIPAddress:80 to access the application


