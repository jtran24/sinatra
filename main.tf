# Configure the Azure provider
terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = ">= 2.26"
    }
  }

  required_version = ">= 0.14.9"
}

provider "azurerm" {
  features {}
}

#Create a resource group name SinatraRGRP in Australia East location

resource "azurerm_resource_group" "SinatraRGRP" {
    name     = "SinatraRGRP"
    location = "Australia East"
}
#Create a Network Security Group named SinatraSG withing the SinatraRGRP
resource "azurerm_network_security_group" "SinatraSG" {
  name                = "SintraSG"
  location            = "AustraliaEast"
  resource_group_name = azurerm_resource_group.SinatraRGRP.name
}

#Create Network Security rule to allow port 80 so we can access the application on port 80
resource "azurerm_network_security_rule" "Port80" {
  name                        = "Allow80"
  priority                    = 102
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "80"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_network_security_group.SinatraSG.resource_group_name
  network_security_group_name = azurerm_network_security_group.SinatraSG.name
}

#Create a Network Security rule to allow port 22 so we can access the VM via ssh
resource "azurerm_network_security_rule" "Port22" {
  name                        = "Allow22"
  priority                    = 103
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "22"
  source_address_prefix       = "*"
  destination_address_prefix  = "*"
  resource_group_name         = azurerm_network_security_group.SinatraSG.resource_group_name
  network_security_group_name = azurerm_network_security_group.SinatraSG.name
}

#Create a Virtual Vnet with the address space of 10.0.0.0/16 and set DNS to use Google DNS so we can browse and download
resource "azurerm_virtual_network" "Sinatra-vnet" {
  name                = "Sinatra-vnet"
  location            = var.location
  resource_group_name = azurerm_resource_group.SinatraRGRP.name
  address_space       = ["10.0.0.0/16"]
  dns_servers         = ["8.8.8.8", "8.8.4.4"]
}

#Create a subnet with the address prefix 10.0.1.0/24
resource "azurerm_subnet" "Sinatra-sub" {
  name                 = "sinatraapp"
  resource_group_name  = azurerm_network_security_group.SinatraSG.resource_group_name
  virtual_network_name = azurerm_virtual_network.Sinatra-vnet.name
  address_prefix       = "10.0.1.0/24"
}

#Create a public IP address so we can later access application from our computer and ssh via internet
resource "azurerm_public_ip" "Sinatra-publicIP" {
  name                = "Sinatra-publicIP"
  location            = var.location
  resource_group_name = azurerm_network_security_group.SinatraSG.resource_group_name
  allocation_method   = "Static"
  ip_version          = "IPv4"
}

#Create a Virtual Machine named SinatraAPPVM  in the same resource group and location with specific VM size
resource "azurerm_virtual_machine" "SinatraAppVM" {
  name                  = "SinatraAPPVM"
  location              = var.location
  resource_group_name   = azurerm_resource_group.SinatraRGRP.name
  network_interface_ids = ["${var.network_interface_id}"]
  vm_size               = "Standard_DS1_v2"

#Specify the OS we want to use
  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }
#Create a disk with replication option 
  storage_os_disk {
    name              = "sinatradisk1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
   
#Specify username and password to access th VM
  os_profile {
    computer_name  = "SinatraAPP01"
    admin_username = "Sinatra01"
    admin_password = "TacosTuesday21!"
    
  }
 
#This allow us to authentication with password if we don't use key authentication
  os_profile_linux_config {
    disable_password_authentication = false
  }
}
